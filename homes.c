#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "homes.h"

// TODO: Write a function called readhomes that takes the filename
// of the homes file and returns an array of pointers to home structs
// terminated with a NULL
struct home** readHomes(char* filename);
int homesCount(struct home** homes);
int zipCount(struct home** homes, int zip);
void simHomes(struct home** homes, int price);

int main(int argc, char *argv[])
{
    if(argc < 2)
    {
        printf("Usage: %s hash_file dict_file\n", argv[0]);
        exit(1);
    }
    // TODO: Use readhomes to get the array of structs
    struct home** homes = readHomes(argv[1]);
    
    // At this point, you should have the data structure built
    // TODO: How many homes are there? Write a function, call it,
    // and print the answer.
    printf("Number of homes: %d\n", homesCount(homes));
    
    // TODO: Prompt the user to type in a zip code.
    int tZip = 0;
    printf("Please enter a zipcode: ");
    scanf("%i", &tZip);
    
    // At this point, the user's zip code should be in a variable
    // TODO: How many homes are in that zip code? Write a function,
    // call it, and print the answer.
    printf("Number of homes in this zipcode: %i\n", zipCount(homes, tZip));
    
    
    // TODO: Prompt the user to type in a price.
    int tPrice = 0;
    printf("Please enter the price of a home: ");
    scanf("%i", &tPrice);
    

    // At this point, the user's price should be in a variable
    // TODO: Write a function to print the addresses and prices of 
    // homes that are within $10000 of that price.
    simHomes(homes, tPrice);
}

struct home** readHomes(char* filename)
{
    FILE *f = fopen(filename, "r");
    if(!f)
    {
        printf("Can't open that file\n");
        exit(1);
    }
    
    int size = 50;
    
    struct home **homes = (struct home**)malloc(size*sizeof(struct home**));
    
    char newAddr[100];
    int tZip, tPrice, tArea;
    int i=0;
    while(fscanf(f, "%i,%[^,],%i,%i", &tZip, newAddr, &tPrice, &tArea) != EOF)
    {
        if(i == size-1)
        {
            size+=10;
            struct home **newHomes = (struct home**)realloc(homes, size*sizeof(struct home*));
            if(newHomes != NULL)homes = newHomes;
            else exit(1);
        }
        struct home *newHome = (struct home*)malloc(sizeof(struct home));
        newHome->zip = tZip;
        char* newStr = (char*)malloc((strlen(newAddr)+1)*sizeof(char));
        strcpy(newStr, newAddr);
        newHome->addr = newStr;
        newHome->price = tPrice;
        newHome->area = tArea;
        homes[i] = newHome;
        i++;
    }
    homes[i] = NULL;
    
    return homes;
}

int homesCount(struct home** homes)
{
    int j=0;
    while(homes[j]!=NULL)
    {
        j++;
    }
    return j;
}

int zipCount(struct home** homes, int zip)
{
    int count = 0;
    int i=0;
    while(homes[i] != NULL)
    {
        if(homes[i]->zip == zip) count++;
        i++;
    }
    return count;
}

void simHomes(struct home** homes, int price)
{
    int i = 0;
    while(homes[i] != NULL)
    {
        if( (homes[i]->price <= price + 10000) & (homes[i]->price >= price - 10000) )
            printf("%s,%i\n", homes[i]->addr, homes[i]->price);
        i++;
    }
}
